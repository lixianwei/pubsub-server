package main

import "gitlab.com/lixianwei/pubsub"

func main() {
	uris := []string{"ws://localhost:5001", "tcp://localhost:5002"}
	pubsub.Server(uris)
}
